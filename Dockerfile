FROM python:3


COPY requirements.txt ./
RUN pip freeze > requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install boto3

RUN mkdir /src

COPY change_urls.py /src/change_urls.py

CMD ["python", "/src/change_urls.py"]