import boto3
import json
import base64
import time
from botocore.exceptions import ClientError
from datetime import datetime, timedelta

# -------------------------------------------------------------- DEFINE GLOBALS
start = time.time()
s3_client = boto3.client('s3')
s3_resource = boto3.Session().resource('s3')
s3_location = '1_RawData/Traffic_HereMaps/'
s3_bucket = 'ev-modeling-platform'
s3_meta_urls = '0_ReferenceData/MetaDataForCode/HereMaps_metaUrls.json'


# ---------------------------------------------------------- UTIL: UPLOAD TO S3
def upload_to_s3(s3_resource, s3_bucket, s3_location, body):
    """ uploads local file to s3
        s3_resource = boto3.Session().resource('s3')
        s3_location = 'data/HereTraffic/tmp'
    """
    s3_resource.Object(s3_bucket, s3_location).put(Body=body)


# ---------------------------------------------- UTIL: NAME OF FOLDER FROM DATE
def date2name(date, format='YYYY_WXX'):
    """date: datetime.date e.g. datetime.now().date()"""
    if format=='YYYY_WXX':
        year = str(date.isocalendar()[0])
        week = str(date.isocalendar()[1]).zfill(2)
        name = year + '_' + 'W' + week
    elif format=='YYYY_MXX':
        year = str(date.isocalendar()[0])
        month = str(date.month).zfill(2)
        name = year + '_' + 'M' + month
    else:
        print('unknown format, return date as string')
        return str(date)
    return name


# ---------------------------------------- UTIL: GET SECRET FROM SecretsManager
def get_secret(secret_name, region_name='eu-central-1'):
    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        else:
            print(e.response)

    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = json.loads(get_secret_value_response['SecretString'])
        else:
            secret = json.loads(
                base64.b64decode(get_secret_value_response['SecretBinary']))
        return secret


# ------------------------------------------------------------------------ MAIN
def main():
    meta_urls_bytes = s3_client.get_object(Bucket=s3_bucket,
                                     Key=s3_meta_urls)
    file_content = meta_urls_bytes['Body'].read().decode('utf-8')
    meta_urls = json.loads(file_content)
    new_urls = {}

    # get API_KEY from SecretsManager
    secret = get_secret(secret_name='HereMaps_API', region_name='eu-central-1')
    API_KEY = secret['API_KEY']

    print('INFO: length of meta_urls: ', len(meta_urls),
          '. Got API_KEY, letters: ', len(API_KEY),
          '. Elapsed time (minutes): ', (time.time()-start)/60)

    # get new timestamps
    today = datetime.today().date()
    offset = (today.weekday() - 2) % 7
    date = today - timedelta(days=offset)  # last wednesday

    year = date.year
    month = date.month
    day = date.day

    # 0900 night - 1630 morning - 2000 noon - 2330 evening
    for hour, minutes in [(9, 0), (16, 30), (23, 30)]:
        timestamp = datetime(year, month, day, hour, minutes, 0)
        time_url = timestamp.strftime('%Y-%m-%dT%H:%M:%S')  # for rest api
        time_png = timestamp.time().strftime('%H%M')  # for unique file name
        print('INFO: Create urls for timestamp: ', time_png)

        for key in meta_urls:
            new_key = key[:-8] + time_png + '.png'
            new_url = meta_urls[key].split('&time=')[0] + '&time=' + time_url
            key_split = new_url.split('<API_KEY>')
            new_url = key_split[0] + API_KEY + key_split[1]
            new_urls.update({new_key: new_url})

    # save new_urls to s3

    uploadByteStream = bytes(json.dumps(new_urls).encode('UTF-8'))
    key = s3_location + date2name(date, format='YYYY_MXX') + '/urls.json'
    bucket = 'ev-modeling-platform'
    print('INFO: upload file to :', key,
          '. total urls: ', len(new_urls),
          '. Elapsed time (minutes): ', (time.time()-start)/60)
    upload_to_s3(s3_resource, s3_bucket=bucket, s3_location=key,
                 body=uploadByteStream)


if __name__ == '__main__':
    main()